import { Controller } from "stimulus"

export default class extends Controller {
    static targets = [ "name" ]
    
    greet() {
	let time = new Date()
	console.log(`Hello hello, ${this.name}! ${time}`)
    }

    get name() {
	return this.nameTarget.value
    }
}
