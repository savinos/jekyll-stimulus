import { Controller } from "stimulus"

export default class extends Controller {
    static targets = [ "current", "what" ]

    connect() {
	this.greet()
	this.startRefreshing()
    }

    
    greet() {
	let currentTime = this.currentTime
	let busesAround = this.busesAround(currentTime)
	let lastBusLeftAt = busesAround[0]
	let nextBusLeavingAt = busesAround[1]

	let durationWhenTheLastBusLeft = this.getInMinutes(currentTime - lastBusLeftAt)
	if (3 <= durationWhenTheLastBusLeft && durationWhenTheLastBusLeft <= 8) {
	    let minutesToBeThere = 10 - durationWhenTheLastBusLeft
	    this.whatTarget.textContent = `Hurry up. Leave now. You should be at Theological School in less than ${minutesToBeThere} minutes.` 
	} else if (durationWhenTheLastBusLeft < 3) {
	    let durationToLeaveFromHouse = 3 - durationWhenTheLastBusLeft
	    this.whatTarget.textContent = `Leave for Theological School bus stop in ${durationToLeaveFromHouse} minutes`
	} else {
	    let durationToLeaveFromHouse = this.getInMinutes(nextBusLeavingAt - currentTime) + 3
	    this.whatTarget.textContent = `Leave for Theological School bus stop in ${durationToLeaveFromHouse} minutes`
	}

	//     this.whatTarget.textContent = `Eftasee`

	// cases
	//  symferei na paw me ta podia (ignore it currently)
	//  mporw na kamw kati kai na paw me to leoforeio
	//  print X time in order to leave from home gia na ftaseis to bus
	//   ean
	//   (current time - time that last bus left) in minutes < (to xrono pou thelei na ftasei sti theologiki - to xrono pou thelw na paw sti theologiki) min || 
    }

    getInMinutes(diffInMilliseconds) {
	let result = Math.round(diffInMilliseconds / 60000)
	return result
    }

    startRefreshing() {
	setInterval(() => {
	    this.greet()
	}, 10000)
    }

    getEstimatedMinutes(route) {
	let estimatedMinutes = {
	    fromHouseToTheologicalSchoolBusStationOnFoot: 5,
	    fromRigenisBusStationToSolomouSquareBusStationOnFoot: 5,
	    fromTheologicalSchoolBusStationToRigenisBusStationByBus: 8,
	    lambdaEnaFromTheBeginningToTheologicalSchoolBusStation: 8,
	    fromHouseToSolomouOnFoot: 15
	}

	let result = estimatedTimes[route]
	
	return result
    }

    busesAround(currentTime) {
	let hours = currentTime.getHours()
	let minutes = currentTime.getMinutes()
	let year = currentTime.getYear()
	let tempTime = new Date(currentTime)
	tempTime.setSeconds(0)
	// TODO special case that we should handle differently
	let prevTime = 0
	for(let tm of this.timetable) {
	    tempTime.setHours(tm[0])
	    tempTime.setMinutes(tm[1])
	    if( tempTime > currentTime) {
	    	return [prevTime, tempTime]
	    }
	    prevTime = new Date(tempTime)
	}
    }


    get nextBus() {
	let time = this.currentTime
	let hours = time.getHours()
	let minutes = time.getMinutes()
	let year = time.getYear()
	let tempTime = new Date(time)
	tempTime.setSeconds(0)
	for(let tm of this.timetable) {
	    tempTime.setHours(tm[0])
	    tempTime.setMinutes(tm[1])
	    if( tempTime > time) {
		// console.log(tempTime)
	    	return tempTime
	    }
	}
    }

    get currentTime() {
	return new Date()
    }

    get timetable() {
	return [
	    [7, 30], [7, 50], [8, 10], [8, 30], [8, 50],
	    [9, 10], [9, 50], [10, 10], [10, 30], [10, 50],
	    [11, 10], [11, 30], [12, 30], [12, 50], [13, 10],
	    [13, 30], [13, 50], [14, 10], [14, 30], [14, 50],
	    [15, 10], [15, 30], [15, 50], [16, 10], [16, 30],
	    [16, 50], [17, 10], [17, 30], [18, 10], [18, 30],
	    [18, 50], [19, 10], [19, 30], [19, 50], [20, 50],
	    [21, 10], [21, 30], [21, 50], [22, 10], [22, 30],
	    [22, 50], [23, 10], [23, 30], [23, 50]
	]
    }

}
