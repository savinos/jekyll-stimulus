# How to play with it

```bash
git clone git@gitlab.com:savinos/jekyll-stimulus.git
cd jekyll-stimulus
bundle
npm install
./node_modules/webpack/bin/webpack.js --config webpack.config.js --progress --profile --bail
jekyll s
```

# Have a look in code

Stimulus controllers code is under `/src/` directory

Stimulus view logic is on `index.html`

# If you are not familiar with those tools...

It's suggested to try getting started part of those from their website, except you like to dive deep from the beginning :)

# Links
	1. [jekyllrb](https://jekyllrb.com/)
	2. [stimulus](https://stimulusjs.org)

# Send me an email

To: <savvas@alexandrou.eu> and tell me your impressions
